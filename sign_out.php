<?php
  	require_once('config.ini'); 
  	require_once('utils/auth.php');

  	//check that it is logged.
  	if(!user_logged_in()) {
      	header("Location: /index.php");
        exit;
  	}
	
	session_start(); 
	$_SESSION = array(); 
	session_destroy();  
	header('location: '.$_SERVER['HTTP_REFERER']);
  exit;
?>
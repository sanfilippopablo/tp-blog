<?php
  require_once('config.ini');
  require_once('utils/forms.php');

  $limit = 5;
  $page = 1;

  if(isset($_GET['page'])){
    if(is_numeric($_GET['page'])){
      $page = $_GET['page'];
    }
  }

  $skip = ($page - 1) * $limit;
  $older = $page + 1;
  $newer = $page - 1;

  $mysqli = get_db_connection();

  if(isset($_GET['submit'])){
    if(isset($_GET['search'])){
      $sql = 'SELECT
                *
              FROM
                `posts`
              WHERE
                `category` LIKE ?
                  OR
                `title` LIKE ?
                  OR
                `short_description` LIKE ?
                  OR
                `html_body` LIKE ?';
      $_GET['search'] = $mysqli->real_escape_string($_GET['search']);
      $search = '%'.$_GET['search'].'%';
      if($stmt = $mysqli->prepare($sql)){
        $stmt->bind_param("ssss", $search, $search, $search, $search);
        if($stmt->execute()){
          $stmt->store_result();
          $all_rows = $stmt->num_rows;
          $stmt->free_result();
        }else{
          $stmt->close();
          $mysqli->close();
          http_response_code(500);
          include('errors/500.html');
          exit();
        }
        $stmt->close();
      }else{
        $mysqli->close();
        http_response_code(500);
        include('errors/500.html');
        exit();
      }
    }
  }elseif(isset($_GET['cat'])){
    $sql = 'SELECT
              *
            FROM
              `posts`
            WHERE
              `category` = ? ';
    $category = $mysqli->real_escape_string($_GET['cat']);
    if($stmt = $mysqli->prepare($sql)){
      $stmt->bind_param("s", $category);
      if($stmt->execute()){
        $stmt->store_result();
        $all_rows = $stmt->num_rows;
        $stmt->free_result();
      }else{
        $stmt->close();
        $mysqli->close();
        http_response_code(500);
        include('errors/500.html');
        exit();
      }
      $stmt->close();
    }else{
      $mysqli->close();
      http_response_code(500);
      include('errors/500.html');
      exit();
    }
  }else{
    $sql = 'SELECT
              *
            FROM
              posts';
    if($result = $mysqli->query($sql)){
      $all_rows = $result->num_rows;
      $result->close();
    }else{
      $mysqli->close();
      http_response_code(500);
      include('errors/500.html');
      exit();
    }
  }
  if(!isset($all_rows)){
    $mysqli->close();
    http_response_code(500);
    include('errors/500.html');
    exit();
  }

  $total_pages = ceil($all_rows/ $limit);

  $sql = 'SELECT
            p.`post_id`, p.`title`, p.`short_description`, p.`published_date`, p.`image`, u.`name`
          FROM
            posts p
          INNER JOIN
            users u
            ON
              p.`user_id` = u.`user_id`';

  if(isset($search)){
    $sql .= ' WHERE
                p.`category` LIKE ?
                  OR
                p.`title` LIKE ?
                  OR
                p.`short_description` LIKE ?
                  OR
                p.`html_body` LIKE ?
              ORDER BY
                p.`published_date` DESC
              LIMIT ? , ? ';
    $search = $mysqli->real_escape_string($search);
    if ($stmt = $mysqli->prepare($sql))
      $stmt->bind_param("ssssii", $search, $search, $search, $search, $skip, $limit);
    else{
      $mysqli->close();
      http_response_code(500);
      include('errors/500.html');
      exit();
    }
  }elseif(isset($category)){
    $sql .= ' WHERE
                p.`category` = ?
              ORDER BY
                p.`published_date` DESC
              LIMIT ? , ? ';
    $category = $mysqli->real_escape_string($category);
    if ($stmt = $mysqli->prepare($sql))
      $stmt->bind_param("sii", $category, $skip, $limit);
    else{
      $mysqli->close();
      http_response_code(500);
      include('errors/500.html');
      exit();
    }
  }else{
    $sql .= ' ORDER BY
                p.`published_date` DESC
              LIMIT ? , ? ';
    if ($stmt = $mysqli->prepare($sql))
      $stmt->bind_param("ii", $skip, $limit);
    else{
      $mysqli->close();
      http_response_code(500);
      include('errors/500.html');
      exit();
    }
  }
  $posts = array();
  if($stmt->execute()){
    $meta = $stmt->result_metadata();
    while ($field = $meta->fetch_field()){
      $pas[] = &$r[$field->name];
    }
    call_user_func_array(array($stmt, 'bind_result'), $pas);
    while ($stmt->fetch()) {
      foreach($r as $key => $val) {
        $c[$key] = $val;
      }
      $posts[] = $c;
    }
    $stmt->close();
  }else{
    $mysqli->close();
    http_response_code(500);
    include('errors/500.html');
    exit();
  }

  $mysqli->close();
?>

<!DOCTYPE html>
<html lang="es">

  <head>
    <meta charset="UTF-8">
    <title><?php echo(APP_NAME) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/index.css">
    <link rel="stylesheet" href="/styles/pagination.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head>

  <body>

    <?php include('templates/header.php') ?>

    <nav class="top-navigation">

      <ul class="categories">
        <li><a href="/index.php?cat=General">General</a></li>
        <li><a href="/index.php?cat=WordPress">WordPress</a></li>
        <li><a href="/index.php?cat=Design">Design</a></li>
        <li><a href="/index.php?cat=Graphics">Graphics</a></li>
        <li><a href="/index.php?cat=Freebies">Freebies</a></li>
        <li><a href="/index.php?cat=Coding">Coding</a></li>
        <li><a href="/index.php?cat=Business">Business</a></li>
      </ul>

      <form action="#" method="get">
        <input id="search" type="search" class="search" name="search" placeholder="Search" value="<?php value_field('search') ?>">
        <input type="hidden" name="submit" value="Search">
      </form>

    </nav>

    <section class="Posts-list">

      <?php
        if(isset($category)){
          echo '<h2 class="posts-list-header">Category: <em>'.$category.'</em></h2>';
        }elseif(isset($_GET['search']) && $_GET['search'] != ''){
          echo '<h2 class="posts-list-header">Search results for <em>'.$_GET['search'].'</em></h2>';
        }
        foreach ($posts as $post) {
      ?>

          <article class="PostItem">
            <h2>
              <a class="PostItem-title" href="/post.php?id=<?php echo $post['post_id'] ?>"><?php echo $post['title'] ?></a>
            </h2>
            <div class="PostItem-details-wrapper">
              <div class="PostItem-image" style="background-image: url('<?php echo $post['image'] ?>')"></div>
              <div class="PostItem-details">
                <p class="PostItem-shortdesc"><?php echo $post['short_description'] ?></p>
                <a class="PostItem-readmorelink" href="/post.php?id=<?php echo $post['post_id'] ?>">Read more &rarr;</a>
              </div>
            </div>
            <div class="PostItem-meta">
              Posted on <?php echo date('d/m/y g:i A',strtotime($post['published_date'])) ?> by <?php echo($post['name']) ?>.
            </div>
          </article>

      <?php
        }

        if($all_rows == 0){
          if(isset($category)){
            echo '<div class="no-results-notice">No results.</div>';
          }elseif($search){
            echo '<div class="no-results-notice">No results.</div>';
          }else{
            echo '<p>No results found</p>';
          }
        }
      ?>
    </section>

    <section class="pagination">

      <?php

        if($page > 1 & isset($_GET['cat'])){          
          echo '<a href="/index.php?page='.$newer.'&cat='.$category.'">&larr; Newer</a>';
        }elseif ($page > 1) {
          echo '<a href="/index.php?page='.$newer.'">&larr; Newer</a>';
        } else {
          echo '<span></span>';
        };

        if ($total_pages != 0){
          echo('<span class="page-number">Page '.$page.' of '.$total_pages.'</span>');
        }

        if($page < $total_pages & isset($_GET['cat'])){
          echo '<a href="/index.php?page='.$older.'&cat='.$category.'">Older &rarr;</a>';
        }elseif ($page < $total_pages) {
          echo '<a href="/index.php?page='.$older.'">Older &rarr;</a>';
        } else {
          echo '<span></span>';
        };
      ?>

    </section>

    <?php include('templates/footer.php') ?>
  </body>
</html>

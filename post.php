<?php
  require_once('config.ini');
  require_once('utils/auth.php');
  require_once('utils/forms.php');

  $mysqli = get_db_connection();

  $errors = array();

  if(isset($_POST['submit_comment'])){
    // VALIDATION CHECKS

    // Comment
    $errors['comment'] = array();

    if (empty($_POST['comment'])) {
      $errors['comment'][] = "Comment cannot be empty.";
    }
    elseif ( validate_length($_POST['comment'], LENGTH_MIN_COMMENT, LENGTH_MAX_COMMENT) ) {
      $errors['comment'][] = "Comment cannot be shorter than ".LENGTH_MIN_COMMENT." or longer than ".LENGTH_MAX_COMMENT." characters.";
    };

    // Post id
    $errors['post_id'] = array();

    if (!isset($_GET['id'])) {
      $errors['post_id'][] = "Internal error try again.";
    };

    // Check if has been an error. If everything is ok, do the thing.
    $ok = true;
    foreach ($errors as $error) {
      if($error) {
        $ok = false;
        break;
      }
    }

    if ($ok) {
      $comment = htmlspecialchars($_POST['comment'], ENT_QUOTES, 'UTF-8');
      $date = date('Y-m-d H:i:s');
      $post_id = $_GET['id'];
      $user_id = $_SESSION['user']['id'];
      $sql = 'INSERT INTO
                `comments`
                (`body`, `date`, `post_id`, `user_id`)
              VALUES
                ( ? , ? , ? , ? )';
      if($stmt = $mysqli->prepare($sql)){
        $stmt->bind_param("ssii", $comment, $date, $post_id, $user_id);
        $stmt->execute();
        if (!$stmt->affected_rows) {
          $stmt->close();
          $mysqli->close();
          http_response_code(500);
          include('errors/500.html');
          exit;
        }
        $stmt->close();
      }else{
        $mysqli->close();
        http_response_code(500);
        include('errors/500.html');
        exit;
      }
    }
  }

  $post_id = is_numeric($_GET['id'])? $_GET['id'] : -1;

  $sql = 'SELECT
            p.`post_id`, p.`html_body`, p.`title`, p.`published_date`, p.`image`, u.`name`
          FROM
              posts p
            INNER JOIN
              users u
            ON
              p.`user_id` = u.`user_id`
          WHERE post_id = ? ';

  if($stmt = $mysqli->prepare($sql)){
    $stmt->bind_param("i", $post_id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows){
      $meta = $stmt->result_metadata();
      while ($field = $meta->fetch_field()){
        $params[] = &$row[$field->name];
      }
      call_user_func_array(array($stmt, 'bind_result'), $params);
      $stmt->fetch();
      foreach($row as $key => $val) {
        $post[$key] = $val;
      }
      $meta->close();
    }
    $stmt->close();
  }else{
    $mysqli->close();
    http_response_code(500);
    include('errors/500.html');
    exit;
  }

  if(!isset($post)){
    $mysqli->close();
    http_response_code(404);
    include('errors/404.html');
    exit;
  }

  $sql = 'SELECT
            c.`comment_id`, c.`date`, c.`body`, u.`name`
          FROM
              comments c
            INNER JOIN
              users u
            ON
              c.`user_id` = u.`user_id`
          WHERE
            `post_id` = ?
          ORDER BY `date` DESC';
  if ($stmt = $mysqli->prepare($sql)) {
    $stmt->bind_param("i", $post['post_id']);
    $stmt->execute();
    $meta = $stmt->result_metadata();
    while ($field = $meta->fetch_field()){
      $paras[] = &$ro[$field->name];
    }
    $comments = null;
    call_user_func_array(array($stmt, 'bind_result'), $paras);
    while ($stmt->fetch()) {
      foreach($ro as $key => $val) {
        $c[$key] = $val;
      }
      $comments[] = $c;
    }
    $meta->close();
    $stmt->close();
  }else{

  }
  $mysqli->close();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/post.css">
    <link rel="shortcut icon" href="/images/logo.gif">

  </head>
  <body>
    <a id="top"></a>

    <?php include('templates/header.php') ?>

    <div class="PostPage">
      <div class="PostPage-meta">Posted on <?php echo date('d/m/y g:i A',strtotime($post['published_date'])) ?> by <?php echo $post['name'] ?>.</div>
      <h1 class="PostPage-title"><?php echo $post['title'] ?></h1>
      <div class="PostPage-image" style="background-image: url('<?php echo($post['image']) ?>')"></div>
      <div class="PostPage-body"><?php echo $post['html_body'] ?></div>
    </div>

    <div class="back-to-top-button">
      <a href="#top">Go back to top ↑</a>
    </div>

    <div class="PostPage-comments">

      <h2 class="PostPage-comments-header">Comments</h2>

      <?php
        if(user_logged_in()){
      ?>

          <form class="PostPage-comments-newcomment" action="<?php echo $_SERVER["PHP_SELF"].'?id='.$post['post_id'] ?>" method="post">
            <div class="field <?php echo(status_class_for_field($errors, 'comment')) ?>">
              <textarea name="comment"></textarea>
              <?php errors_for_field($errors, 'comment') ?>
            </div>
            <button type="submit" name="submit_comment">Submit</button>
          </form>

      <?php
        }else{
      ?>

          <div class="not-logged-notice">
            <a href="/sign_in.php">Sign in</a> or <a href="/sign_up.php">sign up</a> to comment.
          </div>


      <?php
        }
        foreach ($comments as $comment) {
      ?>

          <div class="Comment">
            <div class="Comment-author"><?php echo $comment['name'] ?></div>
            <div class="Comment-date"><?php echo date('d/m/y g:i A',strtotime($comment['date'])) ?></div>
            <div class="Comment-content"><?php echo $comment['body'] ?></div>

            <?php //check if admin.
              if( user_logged_in() & user_is_admin() ) {
            ?>
             
                <div class="Comment-delete">
                  <a href="/delete_comment.php?comment_id=<?php echo $comment['comment_id'] ?>&post_id=<?php echo $post['post_id'] ?>">Delete comment</a>
                </div>
            
            <?php
              }
            ?>

          </div>

      <?php
          }
          if($comments == null){
            echo '<div class="PostPage-comments-nocomment">No comments yet. Do you want to be the first?</div>';
          }
      ?>

    </div>

    <div class="back-to-top-button">
      <a href="#top">Go back to top ↑</a>
    </div>

    <?php include('templates/footer.php') ?>

  </body>
</html>

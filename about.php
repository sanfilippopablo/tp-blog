<?php require_once('config.ini') ?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/about.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head> 
  <body>

    <?php include('templates/header.php') ?>
  
    <div class="main-container">
      <h1>About</h1>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta dolorum laudantium nesciunt sed suscipit architecto dolores minima consequatur magnam veniam neque et, dignissimos quas, reprehenderit ipsam eveniet unde porro iure a asperiores. Asperiores assumenda error, eligendi in quis dolore officiis expedita non vero. Repellat, neque voluptatem ab esse asperiores, doloribus non, pariatur doloremque eveniet sed dolore mollitia! Iure, non, magni.
      </p>
    </div>

    <?php include('templates/footer.php') ?>

  </body>
</html>
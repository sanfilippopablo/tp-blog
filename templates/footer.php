<link rel="stylesheet" property="stylesheet" href="/styles/footer.css">
<footer class="main-footer">
  <div class="quote">Web Design Matters</div>
  <div class="nav">
    <a href="/about.php">About</a> |
    <a href="/contact.php">Contact</a>
  </div>
</footer>

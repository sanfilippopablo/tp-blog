<?php
	/* Acá es necesario usar dirname(__FILE__) y concatenar la ruta
	porque si usamos require_once('../config.php'), header.php es primero
	incluido y luego evaluado el require_once que sigue, por lo tanto requeriría
	la ruta incorrecta.
	*/
	require_once(dirname(__FILE__).'/../config.ini');
	require_once(dirname(__FILE__).'/../utils/auth.php');
?>

<link href="/styles/header.css" property="stylesheet" rel="stylesheet">

<header class="main-header">
	<a class="title" href="/index.php"><h1><?php echo(APP_NAME) ?></h1></a>
	<div class="auth-buttons">

		<?php
			if(user_logged_in()){
				if(isset($_SESSION['user']['name'])){
					echo '<span>Logged in as '.$_SESSION['user']['name'].'</span>';
				}
				if (user_is_admin()) {
					echo '<a href="/admin/index.php">Admin</a>';
				}
				echo '<a href="/sign_out.php">Sign Out</a>';
				}else{
					echo '<a href="/sign_up.php">Sign Up</a>';
					echo '<a href="/sign_in.php">Sign In</a>';
				}
		?>

	</div>
</header>

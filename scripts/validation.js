function addErrorsToField(field, errors) {
  var ul = field.querySelector('.errors');
  if ( !ul ) {
    ul = document.createElement('ul');
    ul.setAttribute('class', 'errors');
    field.appendChild(ul);
  }

  var lis = "";
  for (var i = 0; i < errors.length; i++) {
    lis += '<li>' + errors[i] + '</li>\n';
  }

  ul.innerHTML = lis;

  field.classList.remove('ok');
  field.classList.add('error');

}

function markFieldAsOK(field) {
  field.classList.remove('error');
  field.classList.add('ok');

  try {
    field.querySelector('ul').remove();
  } catch (e) {}

}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function validateField(field) {
  var input = field.querySelector('input');

  var errors = [];

  if ( input.dataset.required ) {
    if ( !input.value ) {
      errors.push('Cannot be empty');
    }
  }

  // && errors.length == 0 comprueba que todavía no se hayan
  // detectado errores, porque sólo queremos mostrar un error
  // por campo.
  if ( input.dataset.maxLength && errors.length == 0) {
    if ( input.value.length >= input.dataset.maxLength ) {
      errors.push('Cannot be larger than ' + input.dataset.maxLength);
    }
  }

  if ( input.dataset.minLength  && errors.length == 0) {
    if ( input.value.length <= input.dataset.minLength ) {
      errors.push('Cannot be shorter than ' + input.dataset.minLength);
    }
  }

  if ( input.dataset.matchField && errors.length == 0 ) {
    const value = document.querySelector(`input[name="${input.dataset.matchField}"]`).value
    if (value != input.value) {
      errors.push('Fields don\'t match')
    }
  }

  if ( input.dataset.type && input.dataset.type === "email" && errors.length == 0) {
    if ( !validateEmail(input.value) ) {
      errors.push('Not a valid e-mail address');
    }
  }

  // Volcar el resultado de la validación en el field.
  if (errors.length != 0) {
    addErrorsToField(field, errors);
    return 1;
  }
  else {
    markFieldAsOK(field);
    return 0;
  };

}

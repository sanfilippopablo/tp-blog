const fields = document.querySelectorAll('form .field')
const form = document.querySelector('form')
form.addEventListener('submit', (e) => {
  e.preventDefault()
  let error = 0
  fields.forEach((field) => {
    error += validateField(field)
  })
  console.log(error)
  if ( error === 0) {
    form.submit()
  }
})

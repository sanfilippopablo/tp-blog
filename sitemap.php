<?php require_once('config.ini') ?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/sitemap.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head>
  <body>
    
    <?php include('templates/header.php') ?>
    
    <h1>Sitemap</h1>
    
    <?php include('templates/footer.php') ?>
  
  </body>
</html>
<?php
  require_once('config.ini');
  require_once('utils/forms.php');
  require_once('utils/auth.php');

  //check that is not logged in.
  if(user_logged_in()) {
      header("Location: /index.php");
      exit;
  }

  $errors = array();

  if($_SERVER['REQUEST_METHOD'] == 'POST'){

    // VALIDATION CHECKS

    $errors['misc'] = array();

    // Name
    $errors['name'] = array();

    if (empty($_POST['name'])) {
      $errors['name'][] = "Name cannot be empty.";
    }
    elseif ( validate_length($_POST['name'], LENGTH_MIN_NAME, LENGTH_MAX_NAME) ) {
      $errors['name'][] = "Name cannot be shorter than ".LENGTH_MIN_NAME." or longer than ".LENGTH_MAX_NAME." characters.";
    };


    // e-mail
    $errors['email'] = array();

    if (empty($_POST['email'])) {
      $errors['email'][] = "Email cannot be empty.";
    }
    elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'][] = "Your email address is not a valid email address.";
    };

    // Password1
    $errors['password1'] = array();

    if (empty($_POST['password1'])) {
      $errors['password1'][] = "Password cannot be empty.";
    }
    elseif ( validate_length($_POST['password1'], LENGTH_MIN_PASSWORD, LENGTH_MAX_PASSWORD) ) {
      $errors['password1'][] = "Password cannot be shorter than ".LENGTH_MIN_PASSWORD." or longer than ".LENGTH_MAX_PASSWORD." characters.";
    };

    //Password2
    $errors['password2'] = array();
    if ($_POST['password1'] !== $_POST['password2']) {
      $errors['password2'][] = "Passwords doesn't match.";
    };

    // Check if has been an error. If everything is ok, do the thing.
    $ok = true;
    foreach ($errors as $error) {
      if($error) {
        $ok = false;
        break;
      }
    }

    if ($ok) {
      $mysqli = get_db_connection();
      $password_hash = password_hash($_POST['password1'], PASSWORD_DEFAULT);
      $name = $mysqli->real_escape_string(strip_tags($_POST['name'], ENT_QUOTES));
      $email = $mysqli->real_escape_string(strip_tags($_POST['email'], ENT_QUOTES));

      // One more validity check: existing e-mail
      $sql = 'SELECT
                *
              FROM
                `users`
              WHERE
                `email` = ? ';
      if ( $stmt = $mysqli->prepare($sql) ) {
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->free_result();
        $stmt->close();
        if ($num_rows) {
          $errors['email'][] = "There's already an account registered with that e-mail address.";
        } else {
          $sql = 'INSERT INTO
                    `users`
                    (`name`, `email`, `password`)
                  VALUES
                    ( ? , ? , ? )';
          if($stmt = $mysqli->prepare($sql)){
            $stmt->bind_param("sss", $name, $email, $password_hash);
            $stmt->execute();
            $id = $stmt->insert_id;
            $stmt->free_result();
            $stmt->close();
            if ($id) {
              $_SESSION['user']['name'] = $name;
              $_SESSION['user']['email'] = $email;
              $_SESSION['user']['id'] = $id;
              $mysqli->close();
              header('Location: /index.php');
              exit;
            } else {
              $errors['misc'][] = "Sorry, your registration failed. Please go back and try again.";
            }
          } else {
            $errors['misc'][] = "Sorry, an error has occurred, try again.";
          }
        }
      } else {
        $errors['misc'][] = "Sorry, an error has occurred, try again.";
      }
      $mysqli->close();
    }
  }

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/form-common.css">
    <link rel="stylesheet" href="/styles/auth-common.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head>
  <body>

    <?php include('templates/header.php') ?>

    <div class="outter-container">
      <div class="inner-container">
        <form action="#" method="post">
          <div class="field <?php echo(status_class_for_field($errors, 'name')) ?>">
            <input
              name="name"
              type="text"
              placeholder="Name"
              value="<?php value_field('name') ?>"
              data-required="true"
              data-min-length="<?php echo(LENGTH_MIN_NAME) ?>"
              data-max-length="<?php echo(LENGTH_MAX_NAME) ?>"
            >
            <?php errors_for_field($errors, 'name') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'email')) ?>">
            <input
              name="email"
              type="email"
              placeholder="Email"
              value="<?php value_field('email') ?>"
              data-required="true"
              data-type="email"
            >
            <?php errors_for_field($errors, 'email') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'password1')) ?>">
            <input
              name="password1"
              type="password"
              placeholder="Password"
              value="<?php value_field('password1') ?>"
              data-min-length="<?php echo(LENGTH_MIN_PASSWORD) ?>"
              data-max-length="<?php echo(LENGTH_MAX_PASSWORD) ?>"
              data-required="true"
            >
            <?php errors_for_field($errors, 'password1') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'password2')) ?>">
            <input
              name="password2"
              type="password"
              placeholder="Repeat password"
              data-required="true"
              data-match-field="password1"
              value="<?php value_field('password2') ?>"

            >
            <?php errors_for_field($errors, 'password2') ?>
          </div>
          <input type="submit" value="Sign Up" class="submit-button">
        </form>

        <?php errors_for_field($errors, 'misc') ?>

        <div class="notice">Already a member? <a href="/sign_in.php">Sign in</a></div>
      </div>
    </div>

    <script src="/scripts/validation.js"></script>
    <script src="/scripts/sign_up.js" type="text/javascript">
    </script>

    <?php include('templates/footer.php') ?>

  </body>
</html>

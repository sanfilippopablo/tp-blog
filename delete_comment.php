<?php
	require_once('config.ini');
  	require_once('utils/auth.php');

  	//check it administrator.
  	if( !(user_logged_in() & user_is_admin() & isset($_GET['comment_id']) & isset($_GET['post_id']))) {
        $mysqli->close();
        http_response_code(403);
        include('errors/403.html');
        exit();
  	}

	$comment_id = is_numeric($_GET['comment_id'])? $_GET['comment_id'] : 0 ;		
	$post_id = is_numeric($_GET['post_id'])? $_GET['post_id'] : 0 ;	
	
	if($comment_id != 0){
		$mysqli = get_db_connection();
		$sql = 'DELETE FROM 
					`comments` 
				WHERE 
					`comment_id` = ? ';
		if ( $stmt = $mysqli->prepare($sql) ) {
			$stmt->bind_param("i", $comment_id);
            $stmt->execute();
    	    if($stmt->affected_rows){
	            $stmt->close();
	            $mysqli->close();
	            header('Location: /post.php?id='.$post_id.'&delete=true');
	            exit;
	        }else{
	        	$stmt->close();
	        	$mysqli->close();
        		http_response_code(500);
        		include('errors/500.html');
        		exit;
	        }
        }else{
        	$mysqli->close();
        	http_response_code(500);
        	include('errors/500.html');
        	exit;
        }
	}else{
        http_response_code(404);
        include('errors/404.html');
        exit;
	}

?>
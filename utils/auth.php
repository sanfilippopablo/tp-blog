<?php 
function user_logged_in() {
	return isset($_SESSION['user']['id']);
};

function user_is_admin() {
	return isset($_SESSION['user']['admin']);
}
?>
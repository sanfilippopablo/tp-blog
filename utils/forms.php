<?php

function errors_for_field($errors, $field) {
  if (isset($errors[$field])) {
    echo('<ul class="errors '.$field.'-errors">');
    foreach($errors[$field] as $error) {
      echo('<li>'.$error.'</li>');
    }
    echo('</ul>');
  }
}

function messages_for_field($messages, $field) {
  if (isset($messages[$field])) {
    echo('<ul class="success '.$field.'-messages">');
    foreach($messages[$field] as $message) {
      echo('<li>'.$message.'</li>');
    }
    echo('</ul>');
  }
}

function status_class_for_field($errors, $field) {
  // Little hack: Check if the form has been submited.
  // If not, return empty string (pristine fields).
  if( isset($_POST["submit"]) ) {
    if ($errors[$field]) {
      return 'error';
    }
    else {
      return 'ok';
    }
  } else {
    return '';
  }
}

function validate_length($value, $min = 0, $max = 100000) {
  return strlen($value) < $min || strlen($value) > $max;
}

function value_field($field) {
  if(isset($_POST[$field])){
    echo $_POST[$field];
  }elseif(isset($_GET[$field])){
    echo $_GET[$field];
  }else{
    echo '';
  }
}

?>
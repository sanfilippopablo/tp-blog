import feedparser
from pprint import pprint
from BeautifulSoup import BeautifulSoup
import requests
import bcrypt
from time import mktime
from datetime import datetime
import mysql.connector

db_params = {
    'user': 'root',
    'password': 'root',
    'database': 'web_design_matters'
}

connection = mysql.connector.connect(**db_params)

def user_id_by_name(name):
    cursor = connection.cursor()
    cursor.execute('SELECT user_id FROM users WHERE users.name = %s', (name,))
    user = cursor.fetchone()
    cursor.close()
    if user:
        return user[0]
    else:
        user = []
        user.append(name)
        username = name.lower().replace(' ', '').encode('ascii', errors='ignore')
        user.append(username + '@gmail.com')
        user.append(bcrypt.hashpw(username + 'pass', bcrypt.gensalt()))

        cursor = connection.cursor()
        cursor.execute('INSERT INTO users (name, email, password) VALUES (%s, %s, %s)', user)
        user_id = cursor.lastrowid

        return user_id


feed = feedparser.parse('http://www.smashingmagazine.com/feed/')

posts = []
for entry in feed.entries:
    print entry["title"]

    post_tuple = []

    post_tuple.append(entry['title'])
    post_tuple.append(user_id_by_name(entry['author']))
    post_tuple.append(entry['tags'][0]['term'])
    post_tuple.append(entry['content'][0]['value'])
    post_tuple.append(datetime.fromtimestamp(mktime(entry['published_parsed'])))

    soup = BeautifulSoup(entry.description)
    post_tuple.append(soup.p.text)
    post_tuple.append(soup.findAll('img')[-1]['src'])

    post_query = """
    INSERT INTO posts (title, user_id, category, html_body, published_date, short_description, image)
    VALUES (%s, %s, %s, %s, %s, %s, %s);
    """
    cursor = connection.cursor()
    cursor.execute(post_query, post_tuple)
    post_id = cursor.lastrowid
    cursor.close()

    # Comments
    for c in feedparser.parse(entry['wfw_commentrss']).entries:
        comment = []
        comment.append(user_id_by_name(c['author']))
        comment.append( datetime.fromtimestamp(mktime(c['published_parsed'])))
        comment.append(c['content'][0]['value'])
        comment.append(post_id)
        comment_query = """
        INSERT INTO comments (user_id, date, body, post_id)
        VALUES (%s, %s, %s, %s);
        """
        cursor = connection.cursor()
        cursor.execute(comment_query, comment)
        cursor.close()

connection.commit()
connection.close()

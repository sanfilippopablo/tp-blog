<?php
	require_once('../config.ini');
 	require_once('../utils/auth.php');

 	//check it administrator.
  	if( !(user_logged_in() & user_is_admin()) ) {
        http_response_code(403);
        include('../errors/403.html');
        exit;
  	}

  	$limit = 4;
  	$page = 1;

  	if(isset($_GET['page'])){
    	if(is_numeric($_GET['page'])){
      		$page = $_GET['page'];
    	}else{
      		header('Location: /index.php');
      		exit;
    	}
  	}

  	$skip = ($page - 1) * $limit;
  	$older = $page + 1;
  	$newer = $page - 1;
  	$mysqli = get_db_connection();
	$sql = 'SELECT
	          *
	        FROM
	          posts';
	if($result = $mysqli->query($sql)){
		$all_rows = $result->num_rows;
	}else{
		$mysqli->close();
        http_response_code(500);
        include('../errors/500.html');
        exit;
	}

  	$total_pages = ceil($all_rows/ $limit);

	$sql = 'SELECT
				p.`post_id`, p.`title`, p.`published_date`, u.`name`
			FROM
				posts p
			INNER JOIN
					users u
				ON
					p.`user_id` = u.`user_id`
			ORDER BY
				p.`published_date` DESC
			LIMIT
				'.$skip.', '.$limit.';';
	if($result = $mysqli->query($sql)){
		while ($post = $result->fetch_assoc()) {
			$posts[] = $post;
		}
		$result->free();
	}else{
		$result->free();
		$mysqli->close();
        http_response_code(500);
        include('../errors/500.html');
        exit;
	}

	if (!isset($posts)) {
		$mysqli->close();
        http_response_code(404);
        include('../errors/404.html');
        exit;
	}
	
	$mysqli->close();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Admin - Posts</title>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway' rel='stylesheet' type='text/css'>
	  	<link rel="stylesheet" href="/styles/normalize.css">
	  	<link rel="stylesheet" href="/styles/admin-posts.css">
		<link rel="stylesheet" href="/styles/pagination.css">
		<link rel="shortcut icon" href="/images/logo.gif">
	</head>
	<body>

		<?php include('../templates/header.php') ?>

		<div class="main-container">

		<div class="posts-header">
		<h2>Posts</h2>
		<a class="Action-button add" href="/admin/post.php">New Post</a>
		</div>
		<section class="Posts">
			<table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Title</th>
						<th>Published Date</th>
						<th>Author</th>
						<th></th>
						<th></th>
					</tr>
				</thead>

				<tbody>

					<?php
						foreach ($posts as $post) {
					?>

							<tr>
								<td><?php echo($post['post_id']) ?></td>
								<td><?php echo($post['title']) ?></td>
								<td><?php echo date('F d, Y, H:i ',strtotime($post['published_date'])) ?></td>
								<td><?php echo($post['name']) ?></td>
								<td>
									<a class="Action-button edit" href="/admin/post.php?id=<?php echo $post['post_id'] ?>">Edit</a>
								</td>
								<td>
									<a class="Action-button delete" href="/admin/delete_post.php?id=<?php echo $post['post_id'] ?>">Delete</a>
								</td>
							</tr>

					<?php
						}
				    ?>

				</tbody>
			</table>
		</section>
		<section class="pagination">

			<?php
		        if ($page > 1) {
		        	echo '<a href="/admin/index.php?page='.$newer.'">&larr; Newer</a>';
		        } else {
							echo '<span></span>';
				}
		        if ($total_pages != 0){
			        echo('<span class="page-number">Page '.$page.' of '.$total_pages.'</span>');
		        }
		        if ($page < $total_pages) {
		          	echo '<a href="/admin/index.php?page='.$older.'">Older &rarr;</a>';
		        } else {
							echo '<span></span>';
				}
	      	?>

	 	</section>

	 	</div>

	<?php include('../templates/footer.php') ?>

	</body>
</html>

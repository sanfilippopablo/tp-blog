<?php

  	require_once('../config.ini');
  	require_once('../utils/auth.php');
  	require_once('../utils/forms.php');

  	//check it administrator.
  	if( !(user_logged_in() & user_is_admin()) ) {
        http_response_code(403);
        include('../errors/403.html');
        exit;
  	}

  	$errors = array();
	$post = array();

    //If there's a ID in $_GET, it means that we are
    // going to edit a post and not to create a new one.
    // We have to preset the fields with the values of this post.
	if(isset($_POST['submit'])){
	    // VALIDATION

		$errors['misc'] = array();

	    // Title
	    $errors['title'] = array();

		if (empty($_POST['title'])) {
			$errors['title'][] = "Title cannot be empty.";
		}elseif ( validate_length($_POST['title'], LENGTH_MIN_TITLE, LENGTH_MAX_TITLE) ) {
			$errors['title'][] = "Title cannot be shorter than ".LENGTH_MIN_TITLE." or longer than ".LENGTH_MAX_TITLE." characters";
		};

	    // Category
	    $errors['category'] = array();

	    if (empty($_POST['category'])) {
			$errors['category'][] = "Category cannot be empty";
	    }elseif ( validate_length($_POST['category'], LENGTH_MIN_CATEGORY, LENGTH_MAX_CATEGORY) ) {
	     	$errors['category'][] = "Category cannot be shorter than ".LENGTH_MIN_CATEGORY." or longer than ".LENGTH_MAX_CATEGORY." characters";
	   	};

	    // Body
	    $errors['body'] = array();

		if (empty($_POST['body'])) {
	    	$errors['body'][] = "Body cannot be empty.";
		}elseif ( validate_length($_POST['body'], LENGTH_MIN_BODY, LENGTH_MAX_BODY) ) {
	      	$errors['body'][] = "Body cannot be shorter than ".LENGTH_MIN_BODY." or longer than ".LENGTH_MAX_BODY." characters";
	    };

	    // Short Description
	    $errors['short_desc'] = array();

	    if (empty($_POST['short_desc'])) {
			$errors['short_desc'][] = "Short description cannot be empty.";
		}elseif ( validate_length($_POST['short_desc'], LENGTH_MIN_S_DESC, LENGTH_MAX_S_DESC) ) {
	      	$errors['short_desc'][] = "Short description cannot be shorter than ".LENGTH_MIN_S_DESC." or longer than ".LENGTH_MAX_S_DESC." characters";
	    };

	    // Image
	    $errors['image'] = array();

	    if (empty($_POST['image'])) {
			$errors['image'][] = "Image cannot be empty";
		}elseif (validate_length($_POST['image'], LENGTH_MIN_IMAGE, LENGTH_MAX_IMAGE) ) {
	      	$errors['image'][] = "Image cannot be shorter than ".LENGTH_MIN_IMAGE." or longer than ".LENGTH_MAX_IMAGE." characters";
	    };

	    // Check if has been an error. If everything is ok, do the thing.
	    $ok = true;
	    foreach ($errors as $error) {
	    	if($error) {
	        	$ok = false;
	        	break;
	      	}
	    }

	    if ($ok) {
			
			$mysqli = get_db_connection();

      		require_once('../utils/Parsedown.php');
      		$p = new ParseDown();

      		$md_body = $_POST['body'];
			$html_body = $p->text($md_body);
			$short_desc = $_POST['short_desc'];
			$title = $mysqli->real_escape_string($_POST['title']);
			$category = $mysqli->real_escape_string($_POST['category']);
			$image = $mysqli->real_escape_string($_POST['image']);
			$date = date('Y-m-d H:i:s');
			$user_id = $_SESSION['user']['id'];
			if(!isset($_SESSION['post']['id'])) {
				$sql = 'INSERT INTO
							posts
							(title, short_description, published_date, category, md_body, html_body, user_id, image)
						VALUES
							( ? , ? , ? , ? , ? , ? , ? , ?);';
				if($stmt = $mysqli->prepare($sql)){
					$stmt->bind_param("ssssssis", $title, $short_desc, $date, $category, $md_body, $html_body, $user_id, $image);
					$stmt->execute();
					if($stmt->affected_rows){
						$stmt->close();
						$mysqli->close();
						header('Location: /admin/index.php?status=insert');
						exit;
					}else{
						$stmt->close();
						$mysqli->close();
    					http_response_code(500);
    					include('../errors/500.html');
    					exit;
					}
				}else{
    				$mysqli->close();
    				http_response_code(500);
    				include('../errors/500.html');
    				exit;
				}
			}else{
				$sql = 'UPDATE
							`posts`
						SET
							`title` = ? ,
							`short_description` = ? ,
							`published_date` = ? ,
							`category` = ? ,
              				`md_body` = ? ,
							`html_body` = ? ,
							`user_id` = ? ,
							`image` = ?
						WHERE
							`post_id` = ? ';
				if($stmt = $mysqli->prepare($sql)){
					$stmt->bind_param('ssssssisi', $title, $short_desc, $date, $category, $md_body, $html_body, $user_id, $image, $_SESSION['post']['id']);
					$stmt->execute();
					if($stmt->affected_rows){
						$stmt->close();
						$mysqli->close();
						header('Location: /admin/index.php?status=edit&post_id='.$_SESSION['post']['id']);
						unset($_SESSION['post']['id']);
						exit;
					}else{
						unset($_SESSION['post']['id']);
						$stmt->close();
						$mysqli->close();
    					http_response_code(500);
    					include('../errors/500.html');
    					exit;
					}
				}else{
					unset($_SESSION['post']['id']);
					$mysqli->close();
    				http_response_code(500);
    				include('../errors/500.html');
    				exit;
				}
			}
		}
	}elseif(isset($_GET['id'])) {

		$_GET['id'] = is_numeric($_GET['id'])? $_GET['id'] : 0;
		if($_GET['id'] != 0){
			$mysqli = get_db_connection();
			$sql = 'SELECT
						p.`post_id`, p.`md_body`, p.`short_description` , p.`title`, p.`category`, p.`published_date`, p.`image`, u.`name`
					FROM
						posts p
							INNER JOIN
						users u
							ON
						p.`user_id` = u.`user_id`
					WHERE
						post_id = ? ';
			if($stmt = $mysqli->prepare($sql)){
              	$stmt->bind_param("i", $_GET['id']);
            	$stmt->execute();
            	$meta = $stmt->result_metadata(); 
				while ($field = $meta->fetch_field()){ 
				$params[] = &$row[$field->name]; 
				}
				call_user_func_array(array($stmt, 'bind_result'), $params); 
				$stmt->fetch(); 
				foreach($row as $key => $val) { 
				  $post[$key] = $val; 
				} 
				$meta->close();
				$stmt->close();
				if(isset($post)){
					$_SESSION['post']['id'] = $post['post_id'];
					$_POST['title'] = htmlentities($post['title']);
					$_POST['body'] = htmlentities($post['md_body']);
					$_POST['short_desc'] = htmlentities($post['short_description']);
					$_POST['image'] = htmlentities($post['image']);
					$_POST['category'] = htmlentities($post['category']);
				} else {
          			$mysqli->close();
    				http_response_code(404);
    				include('../errors/404.html');
    				exit;
        		}
			} else {
        		$mysqli->close();
    			http_response_code(500);
    			include('../errors/500.html');
    			exit;
      		}
		} else {
    		http_response_code(404);
    		include('../errors/404.html');
    		exit;
    	}

	}
?>

<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php $title = isset($_GET['id'])? 'Admin - Post - '.$_GET['id'] : 'Admin - Post - New'; echo $title; ?></title>
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway' rel='stylesheet' type='text/css'>
	  	<link rel="stylesheet" href="/styles/normalize.css">
	  	<link rel="stylesheet" href="/styles/form-common.css">
	  	<link rel="stylesheet" href="/styles/admin-post.css">
	  	<link rel="shortcut icon" href="/images/logo.gif" />
	</head>
	<body>

		<?php include '../templates/header.php' ?>

		<div class="main-container">
			<form method="POST" action="#">
				<div class="field <?php echo(status_class_for_field($errors, 'title')) ?>">
					<input type="text" name="title" value="<?php value_field('title') ?>" placeholder="Title">
					<?php errors_for_field($errors, 'title') ?>
				</div>
				<div class="fields-group">
					<div class="field <?php echo(status_class_for_field($errors, 'category')) ?>">
						<input type="text" name="category" value="<?php value_field('category') ?>" placeholder="Category">
						<?php errors_for_field($errors, 'category') ?>
					</div>
					<div class="field <?php echo(status_class_for_field($errors, 'image')) ?>">
						<input type="text" name="image" value="<?php value_field('image') ?>" placeholder="Image URL">
						<?php errors_for_field($errors, 'image') ?>
					</div>
				</div>
				<div class="field <?php echo(status_class_for_field($errors, 'short_desc')) ?>">
					<textarea name="short_desc" value="" placeholder="Short description"><?php value_field('short_desc') ?></textarea>
					<?php errors_for_field($errors, 'short_desc') ?>
				</div>
				<div class="field body <?php echo(status_class_for_field($errors, 'body')) ?>">
					<textarea name="body" value="" placeholder="Body"><?php value_field('body') ?></textarea>
          <p>
            Note: You can use Markdown syntax here.
          </p>
					<?php errors_for_field($errors, 'body') ?>
				</div>

				<input type="submit" name="submit" class="submit-button" value="Post">
			</form>

			<?php errors_for_field($errors, 'misc') ?>

		</div>

	    <?php include('../templates/footer.php') ?>

	</body>
</html>

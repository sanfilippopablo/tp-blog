<?php
	require_once('../config.ini');
  	require_once('../utils/auth.php');

  	//check it administrator.
  	if( !(user_logged_in() & user_is_admin() & isset($_GET['id']))) {
	    http_response_code(403);
	    include('../errors/403.html');
	    exit;
  	}

	$post_id = is_numeric($_GET['id'])? $_GET['id'] : 0 ;			
	if($post_id != 0){
		$mysqli = get_db_connection();
		$mysqli->autocommit(FALSE);
		$sql = 'DELETE FROM 
					`comments` 
				WHERE 
					`post_id` = ? ';
		if ( $stmt = $mysqli->prepare($sql) ) {
			$stmt->bind_param("i", $post_id);
            $stmt->execute();
            $stmt->close();
    		$sql = 'DELETE FROM 
    					`posts` 
    				WHERE 
    					`post_id` = ? ';
			if ( $stmt = $mysqli->prepare($sql) ) {
				$stmt->bind_param("i", $post_id);
            	$stmt->execute();
				if($mysqli->commit()){
					$stmt->close();
					$mysqli->close();
					header('Location: /admin/index.php?delete=true');
					exit;
				}
			}
		}
		$mysqli->close();
	}else{
		header('Location: /admin/index.php?delete=false');
		exit;
	}
?>
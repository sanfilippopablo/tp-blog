<?php

  require_once('config.ini');
  require_once('utils/forms.php');

  $errors = array();
  $messages = array();

  if(isset($_POST['send'])){
    // VALIDATION CHECKS

    // Name
    $errors['name'] = array();

    if (empty($_POST['name'])) {
      $errors['name'][] = "Name cannot be empty.";
    }
    elseif ( validate_length($_POST['name'], LENGTH_MIN_NAME, LENGTH_MAX_NAME) ) {
      $errors['name'][] = "Name cannot be shorter than ".LENGTH_MIN_NAME." or longer than ".LENGTH_MAX_NAME." characters.";
    };


    // e-mail
    $errors['email'] = array();

    if (empty($_POST['email'])) {
      $errors['email'][] = "Email cannot be empty.";
    }
    elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'][] = "Your email address is not a valid email address.";
    };

    // message
    $errors['message'] = array();

    if (empty($_POST['message'])) {
      $errors['message'][] = "Message cannot be empty.";
    }
    elseif ( validate_length($_POST['message'], LENGTH_MIN_COMMENT, LENGTH_MAX_COMMENT) ) {
      $errors['message'][] = "Message cannot be shorter than ".LENGTH_MIN_COMMENT." or longer than ".LENGTH_MAX_COMMENT." characters.";
    };

    // Check if has been an error. If everything is ok, do the thing.
    $ok = true;
    foreach ($errors as $error) {
      if($error) {
        $ok = false;
        break;
      }
    }

    if($ok){
      $email_to = "adminfrs@webdesignmatters.16mb.com";
      $email_subject = "Contact from the website";
      $email_message = "Detail of form of contact:\n\n";
      $email_message .= "Name: " . $_POST['name'] . "\n";
      $email_message .= "E-mail: " . $_POST['email'] . "\n";
      $email_message .= "Message: " . $_POST['message'] . "\n\n";

      $headers = 'From: '.$_POST['email']."\r\n".
      'Reply-To: '.$_POST['email']."\r\n" .
      'X-Mailer: PHP/' . phpversion();
      if(mail($email_to, $email_subject, $email_message, $headers)){
        $messages['mail'] = array();
        $messages['mail'][] = '¡The form has been sent!';
        unset($_POST['name']);
        unset($_POST['email']);
        unset($_POST['message']);
        unset($_POST['send']);
      }else{
        $errors['mail'] = array();
        $errors['mail'][] = 'Error and the form has not been sent.';
      }
    }
  }

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/form-common.css">
    <link rel="stylesheet" href="/styles/contact.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head>
  <body>
    
    <?php include('templates/header.php') ?>

    <div class="outter-container">

      <div class="inner-container">

        <h2>Contact</h2>

        <p>Send us a comment, a complain, or just say hi. :)</p>
        
        <form method="POST" action="#">
          <div class="field <?php echo(status_class_for_field($errors, 'name')) ?>">
            <input type="text" name="name" value="<?php value_field('name') ?>" placeholder="Name">
            <?php errors_for_field($errors, 'name') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'email')) ?>">
            <input type="text" name="email" value="<?php value_field('email') ?>" placeholder="Email">
            <?php errors_for_field($errors, 'email') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'message')) ?>">
            <textarea name="message" placeholder="Message"><?php value_field('message') ?></textarea>
            <?php errors_for_field($errors, 'message') ?>
          </div>
          <input type="submit" name="send" value="Send" class="submit-button">
        </form>

        <?php errors_for_field($errors, 'mail') ?>
        <?php messages_for_field($messages, 'mail') ?>
      
      </div>
    </div>

    <?php include('templates/footer.php') ?>

  </body>
</html>
<?php
  require_once('config.ini');
  require_once('utils/auth.php');
  require_once('utils/forms.php');

  //check that is not logged in.
  if(user_logged_in()) {
      header("Location: /index.php");
      exit;
  }

  $messages = array();
  $errors = array();

  if($_SERVER['REQUEST_METHOD'] == 'POST'){

    // VALIDATION CHECKS

    $errors['misc'] = array();

    // e-mail
    $errors['email'] = array();

    if (empty($_POST['email'])) {
      $errors['email'][] = "Email cannot be empty.";
    }
    elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errors['email'][] = "Your email address is not a valid email address.";
    };

    // Password
    $errors['password'] = array();

    if (empty($_POST['password'])) {
      $errors['password'][] = "Password cannot be empty.";
    }
    elseif ( validate_length($_POST['password'], LENGTH_MIN_PASSWORD, LENGTH_MAX_PASSWORD) ) {
      $errors['password'][] = "Password cannot be shorter than ".LENGTH_MIN_PASSWORD." or longer than ".LENGTH_MAX_PASSWORD." characters.";
    };

    // Check if has been an error. If everything is ok, do the thing.
    $ok = true;
    foreach ($errors as $error) {
      if($error) {
        $ok = false;
        break;
      }
    }

    if ($ok) {
      $mysqli = get_db_connection();
      $email = $mysqli->real_escape_string($_POST['email']);
      $sql = "SELECT
                name, email, password, user_id, admin
              FROM
                users
              WHERE
                email = ? ";
      if($stmt = $mysqli->prepare($sql)){
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows){
          $meta = $stmt->result_metadata();
          while ($field = $meta->fetch_field()){
            $params[] = &$row[$field->name];
          }
          call_user_func_array(array($stmt, 'bind_result'), $params);
          $stmt->fetch();
          foreach($row as $key => $val) {
            $user[$key] = $val;
          }
          $meta->close();
        }
        $stmt->close();
        if(isset($user)){
          if (password_verify($_POST['password'], $user['password'])) {
            $_SESSION['user']['name'] = $user['name'];
            $_SESSION['user']['email'] = $user['email'];
            $_SESSION['user']['id'] = $user['user_id'];
            $_SESSION['user']['admin'] = $user['admin'];
            $mysqli->close();
            header('Location: /index.php');
            exit;
          }else{
            $errors['misc'][] = 'Wrong password. Try again.';
          }
        }else{
          $errors['misc'][] = 'This user does not exist.';
        }
      }else{
        $mysqli->close();
        http_response_code(500);
        include('errors/500.html');
        exit;
      }
      $mysqli->close();
    }
  }
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title><?php echo APP_NAME ?></title>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic%7CSource+Serif+Pro%7CRoboto+Condensed%7CAlegreya%7CRaleway" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/styles/normalize.css">
    <link rel="stylesheet" href="/styles/form-common.css">
    <link rel="stylesheet" href="/styles/auth-common.css">
    <link rel="shortcut icon" href="/images/logo.gif">
  </head>
  <body>

    <?php include('templates/header.php') ?>

    <div class="outter-container">
      <div class="inner-container">
        <form action="#" method="post">
          <div class="field <?php echo(status_class_for_field($errors, 'email')) ?>">
            <input name="email" type="email" placeholder="Email" data-required="true" value="<?php value_field('email') ?>">
            <?php errors_for_field($errors, 'email') ?>
          </div>
          <div class="field <?php echo(status_class_for_field($errors, 'password')) ?>">
            <input name="password" type="password" placeholder="Password" data-required="true" value="<?php value_field('password') ?>">
            <?php errors_for_field($errors, 'password') ?>
          </div>
          <input type="submit" class="submit-button">
        </form>
        <div class="notice">Not a member? <a href="/sign_up.php">Sign up now</a></div>

        <?php errors_for_field($errors, 'misc') ?>

      </div>
    </div>

    <?php include('templates/footer.php') ?>
    <script src="/scripts/validation.js" type="text/javascript"></script>
    <script src="/scripts/sign_in.js" type="text/javascript"></script>
  </body>
</html>
